import random
import time


class person(object):
	def __init__(self, name, points):
		self.name = name
		self.points = points
	def dice(self):
		self.points += random.randint(2,11)


player1 = person("player1", 0)
computer = person("computer", 0)
passed = False
done = False


def startGame():
	player1.dice()
	ask()


def ask():
	print('Your score is: ', end='')
	print(player1.points)
	answer = input('Would you like to draw, or pass? [d/p] \n')
	if (answer == "d"):
		player1.dice()
		check()
	elif(answer == "p"):
		passed = True
		computerPlay()
	else:
		print('Please type d if you want to draw a card, or p if you want to pass')
		print()
		time.sleep(2)
		ask()


def computerPlay():
	global done
	if (computer.points < 16):
		computer.dice()
		scoreBoard()
		time.sleep(1)
		computerPlay()
	else:
		done = True
		check()


def scoreBoard():
	print("Your score: ", end='')
	print(player1.points)
	print("Computer score: ", end='')
	print(computer.points)


def check():
	if(player1.points > 21):
		print(player1.points)
		print ("\nYou hit over 21 points, sorry")
		input()
	elif(computer.points > 21):
		print("\nYou won! Computer hit over 21 points")
		scoreBoard()
		input()
	elif(player1.points == 21):
		print ("\nYou won, straight 21 points")
		input()
	elif((player1.points > computer.points) and done == True):
		print("\nYou won, gratz!")
		scoreBoard()
		input()
	elif(player1.points == computer.points):
		print("\nA tie!")
		input()
	elif(computer.points > player1.points):
		print("\nSorry, you lost")
		scoreBoard()
		input()
	elif((done == False) and (passed == False)):
		ask()


startGame()
